<?php
/**
 * Account.php - component that renders the account menu on top-lleft of the page.
 * 
 * @author Bugslayer
 * 
 */

/**
 * Checkes wether the request is sent by an authenticated user
 *
 * @return TRUE if the request is sent by an authenticated user, otherwise FALSE
 */
function isAuthenticated() {
	if (! isset ( $_SESSION )) {
		return false;
	}
	return isset ( $_SESSION ['loggedin'] );
}

/**
 * Returns a readable name for the authenticated user
 *
 * @return a readable name for the authenticated user. If the request is not sent by an authenticated user, some
 *         warning message is returned.
 */
function getAuthenticatedUsername() {
	if (! isAuthenticated ()) {
		return "<USER NOT AUTHENTICATED>";
	}
	return $_SESSION ['readablename'];
}

?>

<?php if (isAuthenticated ()) { ?>
<li class="dropdown">
	<a href class="dropdown-toggle"	data-toggle="dropdown"> <?php echo 'Aangemeld: ' . getAuthenticatedUsername() ?> &nbsp;</a>
	<ul class="dropdown-menu" role="menu">
		<li><a href="?action=logoff&page=login">Afmelden</a></li>
	</ul></li>
<?php } else { ?>
	<li><a href="?action=show&page=login">Log in</a></li>
<?php } ?>
<!-- @@project -->

